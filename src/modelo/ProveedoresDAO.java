package modelo;

import Singleton.Singleton;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProveedoresDAO extends DAO<ModeloProveedores>{

    //Contrictor que manda a la super clase Singleton la instancia de la base de datos
    public ProveedoresDAO(Singleton singleton) {
        super(singleton);
    }

    //Metodo sobreescrito de la clase DAO resiviendo la clase ModeloCliente
    @Override
    public void add(ModeloProveedores obj) {
        
        try {

          indice = 1;
          //Instancia que resive una cadena de MySQL con los datos del Modelo de cliente
          ps = conexion.obtenerConexion().prepareCall("insert into proveedores values(0,?,?,?)");
          ps.setString(indice++, obj.getNombre());
          ps.setString(indice++, obj.getDireccion());
          ps.setString(indice++, obj.getTelefono());

          //Instruccion para ejecutar la instruccion de MySQL
          ps.execute();

        } catch (SQLException ex) {
          Logger.getLogger(ProveedoresDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void edit(ModeloProveedores obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(ModeloProveedores obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet consultar(String storedProcedure) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
